﻿using UnityEngine;

namespace Utilities.Extensions
{
    public static class LayerMaskExtensions
    {
        /// <summary>
        ///     Checks whether the given layer is in the layermask.
        /// </summary>
        /// <returns>True if the layer is in the layermask, false otherwise.</returns>
        public static bool Contains(this LayerMask layerMask, int layerToCheck) => layerMask == (layerMask | 1 << layerToCheck);
    }
}
