using System;

using Sirenix.OdinInspector;

using UnityEditor;

using UnityEngine;

public abstract class UpdatableData : ScriptableObject
{
    public bool autoUpdate;
    public event Action OnValuesUpdated;

#if UNITY_EDITOR
    protected virtual void OnValidate()
    {
        if (this.autoUpdate)
            EditorApplication.update += this.NotifyOfUpdatedValues;
    }

    [Button]
    public void NotifyOfUpdatedValues()
    {
        EditorApplication.update -= this.NotifyOfUpdatedValues;
        if (this.OnValuesUpdated != null)
            this.OnValuesUpdated();
    }
#endif
}
