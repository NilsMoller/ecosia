using System.Collections.Generic;

using AI;
using AI.Actions;
using AI.Agents;

using Prototypes.Goap.Data;

using UnityEngine;
using UnityEngine.AI;

using Utilities.PrimitiveReferences;

namespace Prototypes.Goap.Agents
{
    [RequireComponent(typeof(Planner), typeof(WorkerBackpack))]
    public abstract class WorkerBase : MonoBehaviour, IGoapAgent
    {
        [SerializeField] private FloatReference workerInteractionRange = null;
        private NavMeshAgent navmeshAgent;
        protected WorkerBackpack Backpack { get; private set; }

        private void Awake()
        {
            this.Backpack = this.GetComponent<WorkerBackpack>();
            this.navmeshAgent = this.GetComponent<NavMeshAgent>();
            this.navmeshAgent.stoppingDistance = this.workerInteractionRange;
        }

        public UniqueDictionary<Condition, bool> CurrentGoal { get => null; } // this field was added later and is not used in the prototype

        public virtual UniqueDictionary<Condition, bool> GetCurrentState() => new UniqueDictionary<Condition, bool>
        {
            { Condition.HasOre, this.Backpack.AmountOfOre > 0 },
            { Condition.HasLogs, this.Backpack.AmountOfLogs > 0 },
            { Condition.HasTool, this.Backpack.HasTool },
        };

        public abstract UniqueDictionary<Condition, bool> GetGoalState();

        public void OnPlanningComplete(UniqueDictionary<Condition, bool> goal, Queue<ActionBase> actions)
        {
            Utilities.Logger.Log("Plan generated.", actions, "green");
        }

        public virtual void OnPlanCompleted()
        {
            Utilities.Logger.Log("Plan completed.", "green");
        }

        public virtual void OnPlanningFailed(UniqueDictionary<Condition, bool> failedGoal)
        {
            // Not handling this here since we are making sure our goals will always succeed.
            // But normally you want to make sure the world state has changed before running
            // the same goal again, or else it will just fail.
            Utilities.Logger.Log(failedGoal, "red");
        }

        public virtual void OnPlanAborted(ActionBase aborter)
        {
            // An action bailed out of the plan. State has been reset to plan again.
            // Take note of what happened and make sure if you run the same goal again
            // that it can succeed.
            Utilities.Logger.Log("Plan aborted.", aborter, "red");
        }

        public bool MoveAgent(ref ActionBase nextAction)
        {
            if (nextAction.IsInRange)
                return true;

            if (this.navmeshAgent.destination != nextAction.Target)
                this.navmeshAgent.SetDestination(nextAction.Target);

            return false;
        }
    }
}
