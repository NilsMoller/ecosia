using System;
using System.Collections.Generic;
using System.Threading;

using UnityEngine;

using Object = UnityEngine.Object;

namespace Prototypes.WorldGeneration
{
    public class ThreadedDataRequester : MonoBehaviour
    {
        private static ThreadedDataRequester _instance;
        private Queue<ThreadInfo> dataQueue;

        private void Awake()
        {
            this.dataQueue = new Queue<ThreadInfo>();
            ThreadedDataRequester._instance = Object.FindObjectOfType<ThreadedDataRequester>();
        }
    
        private void Update()
        {
            if (this.dataQueue.Count > 0)
                for (int i = 0; i < this.dataQueue.Count; i++)
                {
                    ThreadInfo threadInfo = this.dataQueue.Dequeue();
                    threadInfo.Callback(threadInfo.Parameter);
                }
        }
    
        public static void RequestData(Func<object> generateData, Action<object> callback)
        {
            new Thread(() => ThreadedDataRequester._instance.DataThread(generateData, callback)).Start();
        }

        private void DataThread(Func<object> generateData, Action<object> callback)
        {
            object data = generateData();

            lock (this.dataQueue)
            {
                this.dataQueue.Enqueue(new ThreadInfo(callback, data));
            }
        }

        private readonly struct ThreadInfo
        {
            public readonly Action<object> Callback;
            public readonly object Parameter;

            public ThreadInfo(Action<object> callback, object parameter)
            {
                this.Callback = callback;
                this.Parameter = parameter;
            }
        }
    }
}
