using AI;
using AI.Actions;

using Prototypes.Goap.Data;

using UnityEngine;

using Utilities.PrimitiveReferences;

namespace Prototypes.Goap.Actions.Ore
{
    [DisallowMultipleComponent]
    public class DropOffOreAction : ActionBase
    {
        [SerializeField] private FloatReference workerInteractionRange = null;
        private bool droppedOffOre;
        private SupplyPoint[] supplyPoints;
        private SupplyPoint targetSupplyPoint;

        public override bool IsDone { get => this.droppedOffOre; }
        public override bool RequiresInRange { get => true; }

        public override bool IsInRange
        {
            get
            {
                if (!this.RequiresInRange)
                    return true;

                return Vector3.Distance(this.transform.position, this.targetSupplyPoint.transform.position) < this.workerInteractionRange;
            }
        }

        protected override void Awake()
        {
            base.Awake();

            this.supplyPoints = Object.FindObjectsOfType<SupplyPoint>();

            this.droppedOffOre = false;
            this.targetSupplyPoint = null;

            this.AddPrecondition(Condition.HasOre, true);
            this.AddPostcondition(Condition.HasOre, false);
            this.AddPostcondition(Condition.CollectOre, true);
        }

        protected override void OnReset()
        {
            this.droppedOffOre = false;
            this.targetSupplyPoint = null;
            this.Target = Vector3.zero;
        }

        public override bool CheckProceduralPreconditions()
        {
            SupplyPoint nearestSupplyPoint = null;
            float distanceToNearestSupplyPoint = float.MaxValue;

            foreach (SupplyPoint supplyPointCheck in this.supplyPoints)
            {
                float distance = Vector3.Distance(supplyPointCheck.gameObject.transform.position, this.gameObject.transform.position);
                if (distanceToNearestSupplyPoint > distance)
                {
                    distanceToNearestSupplyPoint = distance;
                    nearestSupplyPoint = supplyPointCheck;
                }
            }

            if (nearestSupplyPoint == null)
                return false;

            this.targetSupplyPoint = nearestSupplyPoint;
            this.Target = nearestSupplyPoint.transform.position;

            return true;
        }

        public override bool Perform()
        {
            WorkerBackpack backpack = this.GetComponent<WorkerBackpack>();
            this.targetSupplyPoint.AmountOfOre += backpack.AmountOfOre;
            this.droppedOffOre = true;
            backpack.AmountOfOre -= backpack.AmountOfOre;
            return true;
        }
    }

}
