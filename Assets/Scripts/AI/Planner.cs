using System.Collections.Generic;
using System.Linq;

using AI.Actions;
using AI.Agents;

using FSM;

using UnityEngine;

using Utilities.PrimitiveReferences;

using Logger = Utilities.Logger;

namespace AI
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(IGoapAgent))]
    public class Planner : MonoBehaviour
    {
        [SerializeField]
        private FloatReference retryFailedPlanningFrequency = null;

        private HashSet<ActionBase> availableActions;
        private Queue<ActionBase> currentActions;

        private float currentRetryFailedPlanningCooldown = 0f;

        private IGoapAgent dataProvider;

        private FiniteStateMachine stateMachine;
        private FiniteStateMachine.FsmState idleState, moveToState, performActionState;

        private void Awake()
        {
            this.dataProvider = this.GetComponent<IGoapAgent>();
            
            this.stateMachine = new FiniteStateMachine();
            this.currentActions = new Queue<ActionBase>();
            this.availableActions = new HashSet<ActionBase>(this.GetComponents<ActionBase>());
        }

        private void Start()
        {
            CreateIdleState();
            CreateMoveToState();
            CreatePerformActionState();
            this.stateMachine.Push(this.idleState); // default to idle

            void CreateIdleState()
            {
                this.idleState = (fsm) =>
                {
                    UniqueDictionary<Condition, bool> worldState = this.dataProvider.GetCurrentState();
                    UniqueDictionary<Condition, bool> goal = this.dataProvider.GetGoalState();

                    Queue<ActionBase> plan = this.Plan(worldState, goal);
                    if (plan != null)
                    {
                        this.currentActions = plan;
                        this.dataProvider.OnPlanningComplete(goal, plan);

                        fsm.Pop();
                        fsm.Push(this.performActionState);
                    }
                    else
                    {
                        this.dataProvider.OnPlanningFailed(goal);
                        fsm.Pop();
                        fsm.Push(this.idleState);
                        this.currentRetryFailedPlanningCooldown = this.retryFailedPlanningFrequency;
                    }
                };
            }

            void CreateMoveToState()
            {
                this.moveToState = (fsm) =>
                {
                    ActionBase action = this.currentActions.Peek();
                    if (this.dataProvider.MoveAgent(ref action))
                        fsm.Pop();
                };
            }

            void CreatePerformActionState()
            {
                this.performActionState = (fsm) =>
                {
                    if (this.currentActions.Count == 0)
                    {
                        Logger.Log("Done actions", "red");
                        fsm.Pop();
                        fsm.Push(this.idleState);
                        this.dataProvider.OnPlanCompleted();
                        return;
                    }

                    ActionBase action = this.currentActions.Peek();

                    if (action.IsDone)
                        this.currentActions.Dequeue(); // the action is done. remove it so we can perform the next one

                    if (this.currentActions.Count > 0)
                    {
                        action = this.currentActions.Peek();
                        if (action.IsInRange)
                        {
                            bool performedSuccessfully = action.Perform();
                            if (performedSuccessfully)
                                return;

                            // action failed. plan again
                            fsm.Pop();
                            fsm.Push(this.idleState);
                            this.dataProvider.OnPlanAborted(action);
                        }
                        else
                        {
                            fsm.Push(this.moveToState);
                        }
                    }

                    // no actions left. move to plan state
                    else
                    {
                        fsm.Pop();
                        fsm.Push(this.idleState);
                        this.dataProvider.OnPlanCompleted();
                    }
                };
            }
        }

        private void Update()
        {
            if (this.currentRetryFailedPlanningCooldown > 0f)
                this.currentRetryFailedPlanningCooldown -= Time.deltaTime;
            else if (this.currentRetryFailedPlanningCooldown <= 0f)
                this.stateMachine.Invoke();
        }

        private void OnDrawGizmosSelected()
        {
            if (this.currentActions != null && this.currentActions.Count > 0)
                Gizmos.DrawSphere(this.currentActions.Peek().Target, 0.5f);
        }

        /// <summary>
        ///     Generate the sequence of actions to execute to fulfill the goal.
        /// </summary>
        /// <returns>Collection of actions to perform in order to fulfill the goal. Null if no plan could be made.</returns>
        private Queue<ActionBase> Plan(UniqueDictionary<Condition, bool> worldState,
                                       UniqueDictionary<Condition, bool> goal)
        {
            foreach (ActionBase action in this.availableActions)
                action.ResetAction();

            HashSet<ActionBase> possibleActions = new HashSet<ActionBase>(this.availableActions.Where(action => action.CheckProceduralPreconditions()));

            List<Node> leaves = new List<Node>();
            Node rootNode = new Node(null, worldState, null);
            bool generatedPlanSuccessfully = this.BuildGraph(
                rootNode,
                leaves,
                possibleActions,
                goal
            );

            if (!generatedPlanSuccessfully)
                return null;

            Node cheapestLeaf = null;
            foreach (Node leaf in leaves.Where(leaf => cheapestLeaf == null || leaf.TotalCost < cheapestLeaf.TotalCost))
                cheapestLeaf = leaf;

            List<ActionBase> result = new List<ActionBase>();
            Node n = cheapestLeaf;
            while (n != null)
            {
                if (n.Action != null)
                    result.Insert(0, n.Action);

                n = n.Parent;
            }

            Queue<ActionBase> plan = new Queue<ActionBase>();
            foreach (ActionBase action in result)
                plan.Enqueue(action);

            return plan;
        }

        /// <summary>
        ///     Builds the complete graph of possible paths to take to reach the goal.
        /// </summary>
        /// <returns>Returns true if one or more valid paths are found. False if no paths were found.</returns>
        /// // todo: return value to exception
        private bool BuildGraph(Node parentNode, ICollection<Node> leaves, HashSet<ActionBase> possibleActions, UniqueDictionary<Condition, bool> goal)
        {
            bool foundAtLeastOnePath = false;

            foreach (ActionBase action in possibleActions)
            {
                // if parent node does not fulfill this action's preconditions, we can't use this. skip this action in that case
                if (action.Preconditions.Except(parentNode.State).Any())
                    continue;

                // apply this action's effects to parent state
                UniqueDictionary<Condition, bool> currentState = new UniqueDictionary<Condition, bool>(parentNode.State);
                foreach (KeyValuePair<Condition, bool> postCondition in action.PostConditions)
                {
                    if (currentState.ContainsKey(postCondition.Key))
                        currentState[postCondition.Key] = postCondition.Value;
                    else
                        currentState.Add(postCondition);
                }

                Node node = new Node(parentNode, currentState, action);
                if (!goal.Except(currentState).Any())
                {
                    leaves.Add(node);
                    foundAtLeastOnePath = true;
                }
                else
                {
                    HashSet<ActionBase> updatedValidActions = new HashSet<ActionBase>(possibleActions);
                    updatedValidActions.Remove(action);
                    bool found = this.BuildGraph(
                        node,
                        leaves,
                        updatedValidActions,
                        goal
                    );
                    if (found)
                        foundAtLeastOnePath = true;
                }
            }

            return foundAtLeastOnePath;
        }

        public void ForceAbortPlan(ActionBase action)
        {
            this.stateMachine.Pop();
            this.stateMachine.Push(this.idleState);
            this.dataProvider.OnPlanAborted(action);
        }

        private class Node
        {

            public Node Parent { get; }
            public float TotalCost { get; }
            public UniqueDictionary<Condition, bool> State { get; }
            public ActionBase Action { get; }

            public Node(Node parent, UniqueDictionary<Condition, bool> state, ActionBase action)
            {
                this.Parent = parent;
                this.TotalCost = parent == null ? 0 : parent.TotalCost + action.Cost;
                this.State = state;
                this.Action = action;
            }
        }
    }
}
