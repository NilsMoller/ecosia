using System.Collections.Generic;
using System.Linq;

using UnityEngine;

using Utilities.PrimitiveReferences;

namespace AI
{
    [DisallowMultipleComponent]
    public class Vision : MonoBehaviour
    {
        private new Collider collider;

        [SerializeField] private FloatReference radius;

        // [SerializeField] private Vector2 offset = Vector2.zero;
        [SerializeField] private LayerMask layersToSee;

        private void Awake()
        {
            this.collider = this.GetComponent<Collider>();
        }

        private void OnValidate()
        {
            // make sure the circle always includes the gameobject
            /*Vector3 position = this.transform.localPosition;
            Vector3 visionCenter = new Vector3(position.x + this.offset.x, position.y, position.z + this.offset.y);
            float distance = Vector3.Distance(visionCenter, position);
            if (distance > this.radius)
            {
                Vector3 originToObject = (visionCenter - position) * this.radius / distance;
                this.offset = new Vector2(originToObject.x, originToObject.z);
            }*/
        }

        private void OnDrawGizmosSelected()
        {
            //Gizmos.DrawWireSphere(new Vector3(currentPosition.x + this.offset.x, currentPosition.y, currentPosition.z + this.offset.y), this.radius);
            Gizmos.DrawWireSphere(this.transform.position, this.radius);
        }

        public IEnumerable<GameObject> See(bool excludeSelf = true)
        {
            /*Vector3 currentPosition = this.transform.position;
            Collider[] seenColliders = Physics.OverlapSphere(new Vector3(currentPosition.x + this.offset.x, currentPosition.y, currentPosition.z + this.offset.y), this.radius, this.layersToSee);*/
            Collider[] seenColliders = Physics.OverlapSphere(this.transform.position, this.radius, this.layersToSee);
            return excludeSelf ? seenColliders.Where(col => col != this.collider).Select(col => col.gameObject).ToArray() : seenColliders.Select(col => col.gameObject);
        }
    }
}
