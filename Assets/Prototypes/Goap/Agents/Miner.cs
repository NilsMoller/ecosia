using System.Collections.Generic;

using AI;

using UnityEngine;

namespace Prototypes.Goap.Agents
{
    [DisallowMultipleComponent]
    public class Miner : WorkerBase
    {
        /// <summary>
        ///     Will only ever mine ore.
        /// </summary>
        public override UniqueDictionary<Condition, bool> GetGoalState() => new UniqueDictionary<Condition, bool>
        {
            { Condition.CollectOre, true },
        };
    }
}
