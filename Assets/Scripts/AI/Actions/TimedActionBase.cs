using Sirenix.OdinInspector;

using UnityEngine;
using UnityEngine.Serialization;

using Utilities.PrimitiveReferences;

namespace AI.Actions
{
    public abstract class TimedActionBase : ActionBase
    {
        [SerializeField]
        [ProgressBar(0f, nameof(TimedActionBase.finalTotalDuration))]
        [ReadOnly]
        private float currentProgress;

        [FormerlySerializedAs("totalWorkDuration")]
        [SerializeField]
        private FloatReference totalDuration = null;

        [SerializeField]
        [Tooltip("Adds a random offset to the total duration. Set to 0 for no random offset.")]
        private FloatReference maxDurationRandomness;

        private float startTime;
        private float finalTotalDuration;

        protected virtual void Update()
        {
            if (this.startTime != 0f && this.currentProgress <= this.finalTotalDuration)
                this.currentProgress = Time.time - this.startTime;
        }

        public override bool Perform()
        {
            if (this.startTime == 0f)
            {
                this.startTime = Time.time;
                this.finalTotalDuration = Mathf.Clamp(this.totalDuration - Random.Range(-this.maxDurationRandomness, this.maxDurationRandomness), 0, float.MaxValue);
            }

            return Time.time - this.startTime > this.finalTotalDuration;
        }

        protected override void OnReset()
        {
            this.startTime = 0f;
            this.currentProgress = 0f;
        }
    }
}
