using System.Collections.Generic;

using AI.Actions;

using UnityEditor;

using UnityEngine;

using Logger = Utilities.Logger;

namespace AI.Agents
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(NavmeshMovement))]
    // todo: add when rabbits eat as well [RequireComponent(typeof(Hunger))]
    public abstract class CreatureBase : MonoBehaviour, IGoapAgent
    {
        private NavmeshMovement movement;
        private ActionBase actionToMoveTo; // for live updated movement
        
        protected Hunger Hunger;

        public abstract UniqueDictionary<Condition, bool> CurrentGoal { get; }

        protected virtual void Awake()
        {
            this.movement = this.GetComponent<NavmeshMovement>();
            this.Hunger = this.GetComponent<Hunger>();
        }

        public virtual UniqueDictionary<Condition, bool> GetCurrentState() => new UniqueDictionary<Condition, bool>
        {
            { Condition.IsThirsty, false }, // todo: add thirst
            { Condition.IsHungry, this.TryGetComponent(out Fox _) && this.Hunger.WantsToEat }, // todo: temp until rabbits eat as well
        };

        protected virtual void Update()
        {
            if (this.actionToMoveTo != null)
                this.movement.SetDestination(this.actionToMoveTo.Target);
        }

        public abstract UniqueDictionary<Condition, bool> GetGoalState();

        public virtual void OnPlanningComplete(UniqueDictionary<Condition, bool> goal, Queue<ActionBase> actions)
        {
            if (Selection.Contains(this.gameObject))
                Logger.Log("Planning completed.", "green", Logger.Stringify(goal) + "\n" + Logger.Stringify(actions));
        }

        public virtual void OnPlanningFailed(UniqueDictionary<Condition, bool> failedGoal)
        {
            if (Selection.Contains(this.gameObject))
                Logger.Log("Planning failed.", failedGoal, "yellow");
        }

        public virtual void OnPlanCompleted()
        {
            if (Selection.Contains(this.gameObject))
                Logger.Log("Plan completed.", "green");
        }

        public virtual void OnPlanAborted(ActionBase aborter)
        {
            if (Selection.Contains(this.gameObject))
                Logger.Log("Plan aborted.", aborter, "red");
        }

        public bool MoveAgent(ref ActionBase nextAction)
        {
            if (nextAction.IsInRange)
                return true;

            this.actionToMoveTo = nextAction;

            return false;
        }
    }
}
