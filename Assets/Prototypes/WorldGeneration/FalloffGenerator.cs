using UnityEngine;

namespace Prototypes.WorldGeneration
{
    public static class FalloffGenerator
    {
        public static float[,] GenerateFalloffMap(int size)
        {
            float[,] map = new float[size, size];

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    float x = i / (float)size * 2 - 1;
                    float y = j / (float)size * 2 - 1;

                    float value = Mathf.Max(Mathf.Abs(x), Mathf.Abs(y));
                    map[i, j] = FalloffGenerator.Evaluate(value);
                }
            }

            return map;
        }

        private static float Evaluate(float value)
        {
            float a = 3f; // 1 for own value, 3 for sebastians value
            float b = 2.2f; // 5.2 for own value, 2.2 for sebastians value

            return Mathf.Pow(value, a) / (Mathf.Pow(value, a) + Mathf.Pow(b - b * value, a));
        }
    }
}
