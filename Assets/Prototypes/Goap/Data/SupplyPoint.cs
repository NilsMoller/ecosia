using UnityEngine;

using Logger = Utilities.Logger;

namespace Prototypes.Goap.Data
{
    public class SupplyPoint : MonoBehaviour
    {
        private int amountOfLogs;
        private int amountOfOre;
        private int amountOfTools;

        public int AmountOfTools
        {
            get => this.amountOfTools;
            set
            {
                Logger.Log($"Tools: {value}", "black");
                this.amountOfTools = value;
            }
        }

        public int AmountOfOre
        {
            get => this.amountOfOre;
            set
            {
                Logger.Log($"Ores {value}", "black");
                this.amountOfOre = value;
            }
        }

        public int AmountOfLogs
        {
            get => this.amountOfLogs;
            set
            {
                Logger.Log($"Logs: {value}", "black");
                this.amountOfLogs = value;
            }
        }

        private void Awake()
        {
            this.amountOfTools = 0;
            this.amountOfOre = 0;
            this.amountOfLogs = 0;
        }
    }
}
