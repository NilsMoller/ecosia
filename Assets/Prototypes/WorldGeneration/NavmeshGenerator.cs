using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.AI;

using Object = UnityEngine.Object;

namespace Prototypes.WorldGeneration
{
    public class NavmeshGenerator : MonoBehaviour
    {
        public event Action OnNavmeshGenerated;

        [HideInInspector]
        public float worldRadius;

        public void StartBuildingNavmesh(IEnumerable<NavMeshSurface> navMeshSurfaces)
        {
            this.StartCoroutine(this.BuildNavmeshCoroutine(navMeshSurfaces));
        }
    
        private IEnumerator BuildNavmeshCoroutine(IEnumerable<NavMeshSurface> navMeshSurfaces)
        {
            foreach (NavMeshSurface surface in navMeshSurfaces)
            {
                NavMesh.RemoveAllNavMeshData();
                surface.BuildNavMesh();
                yield return null;
            }

            this.OnNavmeshGenerated?.Invoke();
            Utilities.Logger.Log("Finished generating navmesh.", "green");
        }
    }
}
