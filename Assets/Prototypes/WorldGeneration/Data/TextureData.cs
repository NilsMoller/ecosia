using System;
using System.Linq;

using UnityEngine;

namespace Prototypes.WorldGeneration.Data
{
    [CreateAssetMenu(menuName = "Terrain Generation/Texture Data")]
    public class TextureData : UpdatableData
    {
        private const int textureSize = 512;
        private const TextureFormat textureFormat = TextureFormat.RGB565;

        public Layer[] layers;
        private float savedMaxHeight;
        private float savedMinHeight;
        
        private static readonly int layerCountShader = Shader.PropertyToID("layerCount");
        private static readonly int baseColorsShader = Shader.PropertyToID("baseColors");
        private static readonly int baseStartHeightsShader = Shader.PropertyToID("baseStartHeights");
        private static readonly int baseBlendsShader = Shader.PropertyToID("baseBlends");
        private static readonly int baseColorStrengthsShader = Shader.PropertyToID("baseColorStrengths");
        private static readonly int baseTextureScalesShader = Shader.PropertyToID("baseTextureScales");
        private static readonly int baseTexturesShader = Shader.PropertyToID("baseTextures");
        private static readonly int minHeightShader = Shader.PropertyToID("minHeight");
        private static readonly int maxHeightShader = Shader.PropertyToID("maxHeight");

        public void ApplyToMaterial(Material material)
        {
            material.SetInt(TextureData.layerCountShader, this.layers.Length);
            material.SetColorArray(TextureData.baseColorsShader, this.layers.Select(layer => layer.tint).ToArray());
            material.SetFloatArray(TextureData.baseStartHeightsShader, this.layers.Select(layer => layer.startHeight).ToArray());
            material.SetFloatArray(TextureData.baseBlendsShader, this.layers.Select(layer => layer.blendStrength).ToArray());
            material.SetFloatArray(TextureData.baseColorStrengthsShader, this.layers.Select(layer => layer.tintStrength).ToArray());
            material.SetFloatArray(TextureData.baseTextureScalesShader, this.layers.Select(layer => layer.textureScale).ToArray());
            Texture2DArray texturesArray = TextureData.GenerateTextureArray(this.layers.Select(layer => layer.texture).ToArray());
            material.SetTexture(TextureData.baseTexturesShader, texturesArray);

            this.UpdateMeshHeights(material, this.savedMinHeight, this.savedMaxHeight);
        }

        public void UpdateMeshHeights(Material material, float minHeight, float maxHeight)
        {
            this.savedMinHeight = minHeight;
            this.savedMaxHeight = maxHeight;
            material.SetFloat(TextureData.minHeightShader, minHeight);
            material.SetFloat(TextureData.maxHeightShader, maxHeight);
        }

        private static Texture2DArray GenerateTextureArray(Texture2D[] textures)
        {
            Texture2DArray textureArray = new Texture2DArray(
                TextureData.textureSize,
                TextureData.textureSize,
                textures.Length,
                TextureData.textureFormat,
                true
            );

            for (int i = 0; i < textures.Length; i++)
                textureArray.SetPixels(textures[i].GetPixels(), i);

            textureArray.Apply();
            return textureArray;
        }

        [Serializable]
        public class Layer
        {
            public Texture2D texture;
            public Color tint;
            [Range(0, 1)] public float tintStrength;
            [Range(0, 1)] public float startHeight;
            [Range(0, 1)] public float blendStrength;
            public float textureScale;
        }
    }
}
