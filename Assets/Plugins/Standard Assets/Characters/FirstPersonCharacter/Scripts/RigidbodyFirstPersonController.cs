using System;
using UnityEngine;

namespace UnityStandardAssets.Characters.FirstPerson
{
    [RequireComponent(typeof (Rigidbody))]
    [RequireComponent(typeof (CapsuleCollider))]
    public class RigidbodyFirstPersonController : MonoBehaviour
    {
        [Serializable]
        public class MovementSettings
        {
            public float forwardSpeed = 8.0f;   // Speed when walking forward
            public float backwardSpeed = 4.0f;  // Speed when walking backwards
            public float strafeSpeed = 4.0f;    // Speed when walking sideways
            public float runMultiplier = 2.0f;   // Speed when sprinting
	        public KeyCode runKey = KeyCode.LeftShift;
            public float jumpForce = 30f;
            public AnimationCurve slopeCurveModifier = new AnimationCurve(new Keyframe(-90.0f, 1.0f), new Keyframe(0.0f, 1.0f), new Keyframe(90.0f, 0.0f));
            [HideInInspector] public float currentTargetSpeed = 8f;

#if !MOBILE_INPUT
            private bool mRunning;
#endif

            public void UpdateDesiredTargetSpeed(Vector2 input)
            {
	            if (input == Vector2.zero) return;
				if (input.x > 0 || input.x < 0)
				{
					//strafe
					this.currentTargetSpeed = this.strafeSpeed;
				}
				if (input.y < 0)
				{
					//backwards
					this.currentTargetSpeed = this.backwardSpeed;
				}
				if (input.y > 0)
				{
					//forwards
					//handled last as if strafing and moving forward at the same time forwards speed should take precedence
					this.currentTargetSpeed = this.forwardSpeed;
				}
#if !MOBILE_INPUT
	            if (Input.GetKey(this.runKey))
	            {
		            this.currentTargetSpeed *= this.runMultiplier;
		            this.mRunning = true;
	            }
	            else
	            {
		            this.mRunning = false;
	            }
#endif
            }

#if !MOBILE_INPUT
            public bool Running
            {
                get { return this.mRunning; }
            }
#endif
        }


        [Serializable]
        public class AdvancedSettings
        {
            public float groundCheckDistance = 0.01f; // distance for checking if the controller is grounded ( 0.01f seems to work best for this )
            public float stickToGroundHelperDistance = 0.5f; // stops the character
            public float slowDownRate = 20f; // rate at which the controller comes to a stop when there is no input
            public bool airControl; // can the user control the direction that is being moved in the air
            [Tooltip("set it to 0.1 or more if you get stuck in wall")]
            public float shellOffset; //reduce the radius by that ratio to avoid getting stuck in wall (a value of 0.1f is nice)
        }


        public Camera cam;
        public MovementSettings movementSettings = new MovementSettings();
        public MouseLook mouseLook = new MouseLook();
        public AdvancedSettings advancedSettings = new AdvancedSettings();


        private Rigidbody mRigidBody;
        private CapsuleCollider mCapsule;
        private float mYRotation;
        private Vector3 mGroundContactNormal;
        private bool mJump, mPreviouslyGrounded, mJumping, mIsGrounded;

        public RigidbodyFirstPersonController(float mYRotation)
        {
            this.mYRotation = mYRotation;
        }

        public Vector3 Velocity
        {
            get { return this.mRigidBody.velocity; }
        }

        public bool Grounded
        {
            get { return this.mIsGrounded; }
        }

        public bool Jumping
        {
            get { return this.mJumping; }
        }

        public bool Running
        {
            get
            {
 #if !MOBILE_INPUT
				return movementSettings.Running;
#else
	            return false;
#endif
            }
        }


        private void Start()
        {
            this.mRigidBody = GetComponent<Rigidbody>();
            this.mCapsule = GetComponent<CapsuleCollider>();
            mouseLook.Init (transform, cam.transform);
        }


        private void Update()
        {
            RotateView();

            if (Input.GetKeyDown(KeyCode.Space) && !this.mJump)
            {
                this.mJump = true;
            }
        }


        private void FixedUpdate()
        {
            GroundCheck();
            Vector2 input = GetInput();

            if ((Mathf.Abs(input.x) > float.Epsilon || Mathf.Abs(input.y) > float.Epsilon) && (advancedSettings.airControl || this.mIsGrounded))
            {
                // always move along the camera forward as it is the direction that it being aimed at
                var transform1 = cam.transform;
                Vector3 desiredMove = transform1.forward*input.y + transform1.right*input.x;
                desiredMove = Vector3.ProjectOnPlane(desiredMove, this.mGroundContactNormal).normalized;

                desiredMove.x = desiredMove.x*movementSettings.currentTargetSpeed;
                desiredMove.z = desiredMove.z*movementSettings.currentTargetSpeed;
                desiredMove.y = desiredMove.y*movementSettings.currentTargetSpeed;
                if (this.mRigidBody.velocity.sqrMagnitude <
                    (movementSettings.currentTargetSpeed*movementSettings.currentTargetSpeed))
                {
                    this.mRigidBody.AddForce(desiredMove*SlopeMultiplier(), ForceMode.Impulse);
                }
            }

            if (this.mIsGrounded)
            {
                this.mRigidBody.drag = 5f;

                if (this.mJump)
                {
                    this.mRigidBody.drag = 0f;
                    var velocity = this.mRigidBody.velocity;
                    velocity = new Vector3(velocity.x, 0f, velocity.z);
                    this.mRigidBody.velocity = velocity;
                    this.mRigidBody.AddForce(new Vector3(0f, movementSettings.jumpForce, 0f), ForceMode.Impulse);
                    this.mJumping = true;
                }

                if (!this.mJumping && Mathf.Abs(input.x) < float.Epsilon && Mathf.Abs(input.y) < float.Epsilon && this.mRigidBody.velocity.magnitude < 1f)
                {
                    this.mRigidBody.Sleep();
                }
            }
            else
            {
                this.mRigidBody.drag = 0f;
                if (this.mPreviouslyGrounded && !this.mJumping)
                {
                    StickToGroundHelper();
                }
            }
            this.mJump = false;
        }


        private float SlopeMultiplier()
        {
            float angle = Vector3.Angle(this.mGroundContactNormal, Vector3.up);
            return movementSettings.slopeCurveModifier.Evaluate(angle);
        }


        private void StickToGroundHelper()
        {
            RaycastHit hitInfo;
            if (Physics.SphereCast(transform.position, this.mCapsule.radius * (1.0f - advancedSettings.shellOffset), Vector3.down, out hitInfo,
                                   ((this.mCapsule.height/2f) - this.mCapsule.radius) +
                                   advancedSettings.stickToGroundHelperDistance, Physics.AllLayers, QueryTriggerInteraction.Ignore))
            {
                if (Mathf.Abs(Vector3.Angle(hitInfo.normal, Vector3.up)) < 85f)
                {
                    this.mRigidBody.velocity = Vector3.ProjectOnPlane(this.mRigidBody.velocity, hitInfo.normal);
                }
            }
        }


        private Vector2 GetInput()
        {
            
            Vector2 input = new Vector2
                {
                    x = Input.GetAxis("Horizontal"),
                    y = Input.GetAxis("Vertical")
                };
			movementSettings.UpdateDesiredTargetSpeed(input);
            return input;
        }


        private void RotateView()
        {
            //avoids the mouse looking if the game is effectively paused
            if (Mathf.Abs(Time.timeScale) < float.Epsilon) return;

            // get the rotation before it's changed
            var transform1 = transform;
            float oldYRotation = transform1.eulerAngles.y;

            mouseLook.LookRotation (transform1, cam.transform);

            if (this.mIsGrounded || advancedSettings.airControl)
            {
                // Rotate the rigidbody velocity to match the new direction that the character is looking
                Quaternion velRotation = Quaternion.AngleAxis(transform.eulerAngles.y - oldYRotation, Vector3.up);
                this.mRigidBody.velocity = velRotation*this.mRigidBody.velocity;
            }
        }

        /// sphere cast down just beyond the bottom of the capsule to see if the capsule is colliding round the bottom
        private void GroundCheck()
        {
            this.mPreviouslyGrounded = this.mIsGrounded;
            RaycastHit hitInfo;
            if (Physics.SphereCast(transform.position, this.mCapsule.radius * (1.0f - advancedSettings.shellOffset), Vector3.down, out hitInfo,
                                   ((this.mCapsule.height/2f) - this.mCapsule.radius) + advancedSettings.groundCheckDistance, Physics.AllLayers, QueryTriggerInteraction.Ignore))
            {
                this.mIsGrounded = true;
                this.mGroundContactNormal = hitInfo.normal;
            }
            else
            {
                this.mIsGrounded = false;
                this.mGroundContactNormal = Vector3.up;
            }
            if (!this.mPreviouslyGrounded && this.mIsGrounded && this.mJumping)
            {
                this.mJumping = false;
            }
        }
    }
}
