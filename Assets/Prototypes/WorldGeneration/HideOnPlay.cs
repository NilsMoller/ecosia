using UnityEngine;

namespace Prototypes.WorldGeneration
{
    public class HideOnPlay : MonoBehaviour
    {
        private void Awake() => this.gameObject.SetActive(false);
    }
}
