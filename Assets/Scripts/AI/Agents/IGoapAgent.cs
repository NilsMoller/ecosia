using System.Collections.Generic;

using AI.Actions;

namespace AI.Agents
{
    /// <summary>
    ///     Implemented by any agent wanting to use GOAP.
    /// </summary>
    public interface IGoapAgent
    {
        /// <summary>
        /// The currently active goal.
        /// </summary>
        public UniqueDictionary<Condition, bool> CurrentGoal { get; }

        /// <summary>
        ///     Get the current state of the agent.
        /// </summary>
        public UniqueDictionary<Condition, bool> GetCurrentState();

        /// <summary>
        ///     Get the desired state of the agent.
        /// </summary>
        public UniqueDictionary<Condition, bool> GetGoalState();

        /// <summary>
        ///     A plan was found for a given goal.
        ///     The agent performs these actions in order.
        /// </summary>
        public void OnPlanningComplete(UniqueDictionary<Condition, bool> goal, Queue<ActionBase> actions);

        /// <summary>
        ///     No sequence of actions could be for the supplied goal.
        ///     You will need to try another goal.
        /// </summary>
        public void OnPlanningFailed(UniqueDictionary<Condition, bool> failedGoal);

        /// <summary>
        ///     All actions are complete and the goal was reached.
        /// </summary>
        public void OnPlanCompleted();

        /// <summary>
        ///     One of the actions caused the plan to abort.
        ///     The action is returned.
        /// </summary>
        public void OnPlanAborted(ActionBase aborter);

        /// <summary>
        ///     Move the agent towards the target in order for the next action to be able to perform.
        /// </summary>
        /// <returns>True if the agent is at the target. False if it is not there yet.</returns>
        public bool MoveAgent(ref ActionBase nextAction);
    }
}
