Shader "Custom/Terrain"
{
    Properties
    {
        testTexture("Texture", 2D) = "white"{}
        testScale("Scale", Float) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        const static int maxLayerCount = 8;
        const static float epsilon = 1E-4;
        
        int layerCount;
        float3 baseColors[maxLayerCount];
        float baseStartHeights[maxLayerCount];
        float baseBlends[maxLayerCount];
        float baseColorStrengths[maxLayerCount];
        float baseTextureScales[maxLayerCount];
        
        float minHeight;
        float maxHeight;

        sampler2D testTexture;
        float testScale;

        UNITY_DECLARE_TEX2DARRAY(baseTextures);
        
        struct Input
        {
            float3 worldPos;
            float3 worldNormal;
        };

        float inverse_lerp(float a, float b, float val)
        {
            return saturate((val-a)/(b-a));
        }

        float3 triplanar(float3 world_pos, float scale, float3 blend_axes, int texture_index)
        {
            float3 scaled_world_pos = world_pos / scale;

            const float3 x_projection = UNITY_SAMPLE_TEX2DARRAY(baseTextures, float3(scaled_world_pos.y, scaled_world_pos.z, texture_index)) * blend_axes.x;
            const float3 y_projection = UNITY_SAMPLE_TEX2DARRAY(baseTextures, float3(scaled_world_pos.x, scaled_world_pos.z, texture_index)) * blend_axes.y;
            const float3 z_projection = UNITY_SAMPLE_TEX2DARRAY(baseTextures, float3(scaled_world_pos.x, scaled_world_pos.y, texture_index)) * blend_axes.z;
            
            return x_projection + y_projection + z_projection;
        }
        
        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            const float height_percent = inverse_lerp(minHeight, maxHeight, IN.worldPos.y);
            float3 blend_axes = abs(IN.worldNormal);
            blend_axes /= blend_axes.x + blend_axes.y + blend_axes.z;
            
            for (int i = 0; i < layerCount; i++)
            {
                const float draw_strength = inverse_lerp(-baseBlends[i] / 2 - epsilon, baseBlends[i] / 2, height_percent - baseStartHeights[i]);

                const float3 base_color = baseColors[i] * baseColorStrengths[i];
                const float3 texture_color = triplanar(IN.worldPos, baseTextureScales[i], blend_axes, i) * (1 - baseColorStrengths[i]);
                
                o.Albedo = o.Albedo * (1 - draw_strength) + (base_color + texture_color) * draw_strength;
            }
        }
        ENDCG
    }
    FallBack "Diffuse"
}
