# Ecosystem Simulation

An ecosystem simulation originally inspired by Sebastian Lague's [video](https://www.youtube.com/watch?v=r_It_X7v-1E&t=82s) on the same topic.
The project was realized in 8 weeks during a minor at Fontys.
