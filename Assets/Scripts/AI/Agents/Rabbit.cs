using System.Collections.Generic;

namespace AI.Agents
{
    public class Rabbit : AdultCreatureBase
    {
        private PredatorAvoidance predatorAvoidance;
        
        protected override void Awake()
        {
            base.Awake();
            this.predatorAvoidance = this.GetComponent<PredatorAvoidance>();
        }

        public override UniqueDictionary<Condition, bool> GetCurrentState()
        {
            UniqueDictionary<Condition, bool> currentState = base.GetCurrentState();
            currentState.Add(Condition.WantsToAvoidPredator, this.predatorAvoidance.SeesPredator);

            return currentState;
        }

        public override UniqueDictionary<Condition, bool> GetGoalState()
        {
            if (this.predatorAvoidance.SeesPredator)
                return new UniqueDictionary<Condition, bool>
                {
                    { Condition.WantsToAvoidPredator, false },
                };

            return base.GetGoalState();
        }
    }
}
