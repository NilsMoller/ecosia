using Sirenix.OdinInspector;

using UnityEngine;
using UnityEngine.Serialization;

using Utilities.PrimitiveReferences;

using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace AI
{
    [DisallowMultipleComponent]
    public class GrowingBaby : MonoBehaviour
    {
        [SerializeField]
        private GameObject adultVersion = null;

        [SerializeField]
        [ReadOnly]
        [ProgressBar(0f, nameof(GrowingBaby.finalTotalDuration))]
        private float currentAgingTime;

        [FormerlySerializedAs("totalWorkDuration")]
        [SerializeField]
        private FloatReference totalTimeToGrowUp = null;

        [SerializeField]
        [Tooltip("Adds or subtracts a random offset to the total time needed. Set to 0 for no random offset.")]
        private FloatReference maxDurationRandomness;

        private float startTime;
        private float finalTotalDuration;

        private void Awake()
        {
            this.startTime = Time.time;
            this.finalTotalDuration = this.totalTimeToGrowUp - Random.Range(-this.maxDurationRandomness, this.maxDurationRandomness);
        }

        private void Update()
        {
            if (this.currentAgingTime <= this.finalTotalDuration)
                this.currentAgingTime = Time.time - this.startTime;
            else
                this.GrowUp();
        }

        private void GrowUp()
        {
            Transform transform1 = this.transform;
            Object.Instantiate(this.adultVersion, transform1.position, transform1.rotation, transform1.parent);
            Object.Destroy(this.gameObject);
        }
    }

}
