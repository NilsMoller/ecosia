using System;

using Sirenix.OdinInspector;

using UnityEngine;

namespace Utilities.PrimitiveReferences
{
    [Serializable]
    [InlineProperty]
    public class FloatReference
    {
        [SerializeField]
        [HideLabel]
        [HorizontalGroup(5f)]
        /*[ValueDropdown(
                nameof(FloatReference.GetDropdownValues),
                AppendNextDrawer = true,
                DropdownWidth = 130
            )]*/
        private bool overrideValue = false;

        [SerializeField]
        [HideLabel]
        [HorizontalGroup]
        [HideIf(nameof(FloatReference.overrideValue))]
        private FloatValue floatValue = null;

        [SerializeField]
        [HideLabel]
        [HorizontalGroup]
        [ShowIf(nameof(FloatReference.overrideValue))]
        private float overridenValue = 0f;

        /*private static IEnumerable GetDropdownValues() => new ValueDropdownList<bool>
        {
            { "Reference value", false },
            { "Overriden value", true },
        };*/

        public static implicit operator float(FloatReference floatRef) => floatRef.overrideValue ? floatRef.overridenValue : floatRef.floatValue;
    }

}
