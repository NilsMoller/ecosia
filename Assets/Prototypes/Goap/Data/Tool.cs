using System;

namespace Prototypes.Goap.Data
{
    [Serializable]
    public class Tool
    {

        public Tool(int maxDurability)
        {
            this.Durability = maxDurability;
        }

        public int Durability { get; set; }

        public override string ToString() => $"Tool with {this.Durability} durability.";
    }
}
