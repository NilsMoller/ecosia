using AI;
using AI.Actions;

using Prototypes.Goap.Data;

using UnityEngine;

using Utilities.PrimitiveReferences;

namespace Prototypes.Goap.Actions.Wood
{
    [DisallowMultipleComponent]
    public class ChopTreeAction : TimedActionBase
    {

        [SerializeField] private FloatReference workerInteractionRange = null;
        private bool chopped;
        private TreeTrunk targetTree;
        private TreeTrunk[] trees;

        public override bool RequiresInRange { get => true; }

        public override bool IsInRange
        {
            get
            {
                if (!this.RequiresInRange)
                    return true;

                return Vector3.Distance(this.transform.position, this.targetTree.transform.position) < this.workerInteractionRange;
            }
        }

        public override bool IsDone { get => this.chopped; }

        protected override void Awake()
        {
            base.Awake();
            this.trees = Object.FindObjectsOfType<TreeTrunk>();

            this.chopped = false;
            this.targetTree = null;

            this.AddPrecondition(Condition.HasTool, true);
            this.AddPrecondition(Condition.HasLogs, false);

            this.AddPostcondition(Condition.HasLogs, true);
        }

        protected override void OnReset()
        {
            base.OnReset();
            this.chopped = false;
            this.targetTree = null;
        }

        public override bool CheckProceduralPreconditions()
        {
            TreeTrunk nearestTree = null;
            float distanceToNearestTree = float.MaxValue;

            foreach (TreeTrunk treeCheck in this.trees)
            {
                if (treeCheck.IsClaimed || !treeCheck.IsGrown)
                    continue;

                float distance = Vector3.Distance(treeCheck.gameObject.transform.position, this.gameObject.transform.position);
                if (distanceToNearestTree > distance)
                {
                    distanceToNearestTree = distance;
                    nearestTree = treeCheck;
                }
            }

            if (nearestTree == null)
                return false;

            this.targetTree = nearestTree;
            this.Target = nearestTree.transform.position;
            this.targetTree.IsClaimed = true;

            return true;
        }

        public override bool Perform()
        {
            if (!base.Perform()) // if the timer hasnt finished
                return true;

            // finished chopping
            this.targetTree.Harvest();
            WorkerBackpack backpack = this.GetComponent<WorkerBackpack>();
            const int amountOfLogsGained = 1;
            backpack.AmountOfLogs += amountOfLogsGained;
            backpack.ReduceToolDurability(amountOfLogsGained);
            this.chopped = true;

            return true;
        }
    }
}
