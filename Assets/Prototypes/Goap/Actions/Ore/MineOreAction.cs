using AI;
using AI.Actions;

using Prototypes.Goap.Data;

using UnityEngine;

using Utilities.PrimitiveReferences;

namespace Prototypes.Goap.Actions.Ore
{
    [DisallowMultipleComponent]
    public class MineOreAction : TimedActionBase
    {
        [SerializeField] private FloatReference workerInteractionRange = null;
        private bool mined;
        private Data.Ore[] ores;
        private Data.Ore targetOre;

        public override bool RequiresInRange { get => true; }

        public override bool IsInRange
        {
            get
            {
                if (!this.RequiresInRange)
                    return true;

                return Vector3.Distance(this.transform.position, this.targetOre.transform.position) < this.workerInteractionRange;
            }
        }

        public override bool IsDone { get => this.mined; }

        private new void Awake()
        {
            base.Awake();
            this.ores = Object.FindObjectsOfType<Data.Ore>();

            this.mined = false;
            this.targetOre = null;

            this.AddPrecondition(Condition.HasTool, true);
            this.AddPrecondition(Condition.HasOre, false);

            this.AddPostcondition(Condition.HasOre, true);
        }

        protected override void OnReset()
        {
            base.OnReset();
            this.mined = false;
            this.targetOre = null;
        }

        public override bool CheckProceduralPreconditions()
        {
            Data.Ore nearestOre = null;
            float distanceToNearestOre = float.MaxValue;

            foreach (Data.Ore oreCheck in this.ores)
            {
                if (oreCheck.IsClaimed || !oreCheck.IsGrown)
                    continue;

                float distance = Vector3.Distance(oreCheck.gameObject.transform.position, this.gameObject.transform.position);
                if (distanceToNearestOre > distance)
                {
                    distanceToNearestOre = distance;
                    nearestOre = oreCheck;
                }
            }

            if (nearestOre == null)
                return false;

            this.targetOre = nearestOre;
            this.Target = nearestOre.transform.position;
            this.targetOre.IsClaimed = true;

            return true;
        }

        public override bool Perform()
        {
            if (!base.Perform()) // if the timer hasnt finished
                return true;

            this.targetOre.Harvest();
            WorkerBackpack backpack = this.GetComponent<WorkerBackpack>();
            const int amountOfOreGained = 2;
            backpack.AmountOfOre += amountOfOreGained;
            backpack.ReduceToolDurability(amountOfOreGained);
            this.mined = true;

            return true;
        }
    }

}
