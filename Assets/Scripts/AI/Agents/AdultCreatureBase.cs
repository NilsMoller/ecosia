using System.Collections.Generic;

using AI.Actions;

namespace AI.Agents
{
    public abstract class AdultCreatureBase : CreatureBase
    {
        private Reproduction reproduction;
        private UniqueDictionary<Condition, bool> currentGoal;
        private bool failedToReproduce;
        private bool failedToEat;

        public override UniqueDictionary<Condition, bool> CurrentGoal { get => this.currentGoal; }

        protected override void Awake()
        {
            base.Awake();
            this.reproduction = this.GetComponent<Reproduction>();
            this.currentGoal = new UniqueDictionary<Condition, bool>();
        }

        // todo: make it so we can return multiple goals and pick the one with lowest cost
        public override UniqueDictionary<Condition, bool> GetGoalState()
        {
            UniqueDictionary<Condition, bool> goal = new UniqueDictionary<Condition, bool>
            {
                { Condition.IsIdle, true },
            };

            if (!this.failedToReproduce && this.reproduction.WantsToReproduce)
                goal = new UniqueDictionary<Condition, bool>
                {
                    { Condition.WantsToReproduce, false },
                };

            if (this.gameObject.TryGetComponent(out Fox _) && !this.failedToEat && this.Hunger.WantsToEat) // todo: temp until rabbits eat as well
                goal = new UniqueDictionary<Condition, bool>
                {
                    { Condition.IsHungry, false },
                };

            this.currentGoal = goal;

            return goal;
        }

        public override UniqueDictionary<Condition, bool> GetCurrentState()
        {
            UniqueDictionary<Condition, bool> currentState = base.GetCurrentState();
            currentState.Add(Condition.WantsToReproduce, this.reproduction.WantsToReproduce);

            return currentState;
        }

        public override void OnPlanningFailed(UniqueDictionary<Condition, bool> failedGoal)
        {
            base.OnPlanningFailed(failedGoal);
            if (failedGoal.ContainsKey(Condition.WantsToReproduce))
                this.failedToReproduce = true;
            if (failedGoal.ContainsKey(Condition.IsHungry))
                this.failedToEat = true;
        }

        public override void OnPlanningComplete(UniqueDictionary<Condition, bool> goal, Queue<ActionBase> actions)
        {
            base.OnPlanningComplete(goal, actions);
            this.failedToReproduce = false;
            this.failedToEat = false;
        }
    }

}
