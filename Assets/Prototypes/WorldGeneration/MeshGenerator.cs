using Prototypes.WorldGeneration.Data;

using UnityEngine;

namespace Prototypes.WorldGeneration {
    public static class MeshGenerator
    {

        public static MeshData GenerateTerrainMesh(float[,] heightMap, MeshSettings meshSettings, int levelOfDetail)
        {
            int skipIncrement = levelOfDetail == 0 ? 1 : levelOfDetail * 2;
            int numVertsPerLine = meshSettings.NumVertsPerLine;

            Vector2 topLeft = new Vector2(-1, 1 * meshSettings.MeshWorldSize / 2f);

            MeshData meshData = new MeshData(numVertsPerLine, skipIncrement, meshSettings.useFlatShading);

            int[,] vertexIndicesMap = new int[numVertsPerLine, numVertsPerLine];
            int meshVertexIndex = 0;
            int outOfMeshVertexIndex = -1;

            for (int y = 0; y < numVertsPerLine; y++)
            {
                for (int x = 0; x < numVertsPerLine; x++)
                {
                    bool isOutOfMeshVertex = y == 0 || y == numVertsPerLine - 1 || x == 0 || x == numVertsPerLine - 1;
                    bool isSkippedVertex = x > 2 && x < numVertsPerLine - 3 && y > 2 && y < numVertsPerLine - 3 && ((x - 2) % skipIncrement != 0 || (y - 2) % skipIncrement != 0);
                    if (isOutOfMeshVertex)
                    {
                        vertexIndicesMap[x, y] = outOfMeshVertexIndex;
                        outOfMeshVertexIndex--;
                    }
                    else if (!isSkippedVertex)
                    {
                        vertexIndicesMap[x, y] = meshVertexIndex;
                        meshVertexIndex++;
                    }
                }
            }

            for (int y = 0; y < numVertsPerLine; y++)
            {
                for (int x = 0; x < numVertsPerLine; x++)
                {
                    bool isSkippedVertex = x > 2 && x < numVertsPerLine - 3 && y > 2 && y < numVertsPerLine - 3 && ((x - 2) % skipIncrement != 0 || (y - 2) % skipIncrement != 0);

                    if (isSkippedVertex)
                        continue;
                    
                    bool isOutOfMeshVertex = y == 0 || y == numVertsPerLine - 1 || x == 0 || x == numVertsPerLine - 1;
                    bool isMeshEdgeVertex = y == 1 || y == numVertsPerLine - 2 || x == 1 || x == numVertsPerLine - 2 && !isOutOfMeshVertex;
                    bool isMainVertex = (x - 2) % skipIncrement == 0 && (y - 2) % skipIncrement == 0 && !isOutOfMeshVertex && !isMeshEdgeVertex;
                    bool isEdgeConnectionVertex = (y == 2 || y == numVertsPerLine - 3 || x == 2 || x == numVertsPerLine - 3) && !isOutOfMeshVertex && !isMeshEdgeVertex && !isMainVertex;

                    int vertexIndex = vertexIndicesMap[x, y];
                    Vector2 percent = new Vector2(x - 1, y - 1) / (numVertsPerLine - 3);
                    float height = heightMap[x, y];
                    Vector2 vertexPosition2D = topLeft + new Vector2(percent.x, -percent.y) * meshSettings.MeshWorldSize;

                    if (isEdgeConnectionVertex)
                    {
                        bool isVertical = x == 2 || x == numVertsPerLine - 3;
                        int dstToMainVertexA = ((isVertical ? y : x) - 2) % skipIncrement;
                        int dstToMainVertexB = skipIncrement - dstToMainVertexA;
                        float dstPercentFromAToB = dstToMainVertexA / (float)skipIncrement;

                        float heightMainVertexA = heightMap[isVertical ? x : x - dstToMainVertexA, isVertical ? y - dstToMainVertexA : y];
                        float heightMainVertexB = heightMap[isVertical ? x : x + dstToMainVertexB, isVertical ? y + dstToMainVertexB : y];

                        height = heightMainVertexA * (1 - dstPercentFromAToB) + heightMainVertexB * dstPercentFromAToB;
                    }

                    meshData.AddVertex(new Vector3(vertexPosition2D.x, height, vertexPosition2D.y), percent, vertexIndex);

                    bool createTriangle = x < numVertsPerLine - 1 && y < numVertsPerLine - 1 && (!isEdgeConnectionVertex || x != 2 && y != 2);

                    if (createTriangle)
                    {
                        int currentIncrement = isMainVertex && x != numVertsPerLine - 3 && y != numVertsPerLine - 3 ? skipIncrement : 1;

                        int a = vertexIndicesMap[x, y];
                        int b = vertexIndicesMap[x + currentIncrement, y];
                        int c = vertexIndicesMap[x, y + currentIncrement];
                        int d = vertexIndicesMap[x + currentIncrement, y + currentIncrement];
                        meshData.AddTriangle(a, d, c);
                        meshData.AddTriangle(d, a, b);
                    }
                }
            }

            meshData.FinalizeMesh();

            return meshData;
        }
    }

    public class MeshData
    {
        private readonly int[] outOfMeshTriangles;

        private readonly Vector3[] outOfMeshVertices;
        private readonly int[] triangles;

        private readonly bool useFlatShading;
        private Vector3[] bakedNormals;
        private int outOfMeshTriangleIndex;

        private int triangleIndex;
        private Vector2[] uvs;
        private Vector3[] vertices;

        public MeshData(int numVertsPerLine, int skipIncremement, bool useFlatShading)
        {
            this.useFlatShading = useFlatShading;

            int numMeshEdgeVertices = (numVertsPerLine - 2) * 4 - 4;
            int numEdgeConnectionVertices = (skipIncremement - 1) * (numVertsPerLine - 5) / skipIncremement * 4;
            int numMainVerticesPerLine = (numVertsPerLine - 5) / skipIncremement + 1;
            int numMainVertices = numMainVerticesPerLine * numMainVerticesPerLine;

            this.vertices = new Vector3[numMeshEdgeVertices + numEdgeConnectionVertices + numMainVertices];
            this.uvs = new Vector2[this.vertices.Length];

            int numMeshEdgeTriangles = 8 * (numVertsPerLine - 4); // ((numVertsPerLine - 3) * 4 - 4) * 2
            int numMainTriangles = (numMainVerticesPerLine - 1) * (numMainVerticesPerLine - 1) * 2;
            this.triangles = new int[(numMeshEdgeTriangles + numMainTriangles) * 3];

            this.outOfMeshVertices = new Vector3[numVertsPerLine * 4 - 4];
            this.outOfMeshTriangles = new int[24 * (numVertsPerLine - 2)]; // ((numVertsPerLine - 1) * 4 - 4) * 2 * 3
        }

        public void AddVertex(Vector3 vertexPosition, Vector2 uv, int vertexIndex)
        {
            if (vertexIndex < 0)
            {
                this.outOfMeshVertices[-vertexIndex - 1] = vertexPosition;
            }
            else
            {
                this.vertices[vertexIndex] = vertexPosition;
                this.uvs[vertexIndex] = uv;
            }
        }

        public void AddTriangle(int a, int b, int c)
        {
            if (a < 0 || b < 0 || c < 0)
            {
                this.outOfMeshTriangles[this.outOfMeshTriangleIndex] = a;
                this.outOfMeshTriangles[this.outOfMeshTriangleIndex + 1] = b;
                this.outOfMeshTriangles[this.outOfMeshTriangleIndex + 2] = c;
                this.outOfMeshTriangleIndex += 3;
            }
            else
            {
                this.triangles[this.triangleIndex] = a;
                this.triangles[this.triangleIndex + 1] = b;
                this.triangles[this.triangleIndex + 2] = c;
                this.triangleIndex += 3;
            }
        }

        private Vector3[] CalculateNormals()
        {
            Vector3[] vertexNormals = new Vector3[this.vertices.Length];
            int triangleCount = this.triangles.Length / 3;
            for (int i = 0; i < triangleCount; i++)
            {
                int normalTriangleIndex = i * 3;
                int vertexIndexA = this.triangles[normalTriangleIndex];
                int vertexIndexB = this.triangles[normalTriangleIndex + 1];
                int vertexIndexC = this.triangles[normalTriangleIndex + 2];

                Vector3 triangleNormal = this.SurfaceNormalFromIndices(vertexIndexA, vertexIndexB, vertexIndexC);
                vertexNormals[vertexIndexA] += triangleNormal;
                vertexNormals[vertexIndexB] += triangleNormal;
                vertexNormals[vertexIndexC] += triangleNormal;
            }

            int borderTriangleCount = this.outOfMeshTriangles.Length / 3;
            for (int i = 0; i < borderTriangleCount; i++)
            {
                int normalTriangleIndex = i * 3;
                int vertexIndexA = this.outOfMeshTriangles[normalTriangleIndex];
                int vertexIndexB = this.outOfMeshTriangles[normalTriangleIndex + 1];
                int vertexIndexC = this.outOfMeshTriangles[normalTriangleIndex + 2];

                Vector3 triangleNormal = this.SurfaceNormalFromIndices(vertexIndexA, vertexIndexB, vertexIndexC);
                if (vertexIndexA >= 0)
                    vertexNormals[vertexIndexA] += triangleNormal;
                if (vertexIndexB >= 0)
                    vertexNormals[vertexIndexB] += triangleNormal;
                if (vertexIndexC >= 0)
                    vertexNormals[vertexIndexC] += triangleNormal;
            }

            for (int i = 0; i < vertexNormals.Length; i++)
                vertexNormals[i].Normalize();

            return vertexNormals;
        }

        private Vector3 SurfaceNormalFromIndices(int indexA, int indexB, int indexC)
        {
            Vector3 pointA = indexA < 0 ? this.outOfMeshVertices[-indexA - 1] : this.vertices[indexA];
            Vector3 pointB = indexB < 0 ? this.outOfMeshVertices[-indexB - 1] : this.vertices[indexB];
            Vector3 pointC = indexC < 0 ? this.outOfMeshVertices[-indexC - 1] : this.vertices[indexC];

            Vector3 sideAB = pointB - pointA;
            Vector3 sideAC = pointC - pointA;

            return Vector3.Cross(sideAB, sideAC).normalized;
        }

        public void FinalizeMesh()
        {
            if (this.useFlatShading)
                this.FlatShading();
            else
                this.BakeNormals();
        }

        private void BakeNormals()
        {
            this.bakedNormals = this.CalculateNormals();
        }

        private void FlatShading()
        {
            Vector3[] flatShadedVertices = new Vector3[this.triangles.Length];
            Vector2[] flatShadedUvs = new Vector2[this.triangles.Length];

            for (int i = 0; i < this.triangles.Length; i++)
            {
                flatShadedVertices[i] = this.vertices[this.triangles[i]];
                flatShadedUvs[i] = this.uvs[this.triangles[i]];
                this.triangles[i] = i;
            }

            this.vertices = flatShadedVertices;
            this.uvs = flatShadedUvs;
        }

        public Mesh CreateMesh()
        {
            Mesh mesh = new Mesh
            {
                vertices = this.vertices,
                triangles = this.triangles,
                uv = this.uvs,
            };

            if (this.useFlatShading)
                mesh.RecalculateNormals();
            else
                mesh.normals = this.bakedNormals;

            return mesh;
        }
    }
}