using System;
using System.Collections.Generic;
using System.Linq;

using Prototypes.WorldGeneration.Data;

using UnityEngine;
using UnityEngine.AI;

namespace Prototypes.WorldGeneration
{
    [RequireComponent(typeof(NavmeshGenerator))]
    public class TerrainGenerator : MonoBehaviour
    {
        private const float viewerMoveThresholdForChunkUpdate = 25f;
        private const float sqrViewerMoveThresholdForChunkUpdate = TerrainGenerator.viewerMoveThresholdForChunkUpdate * TerrainGenerator.viewerMoveThresholdForChunkUpdate;

        [SerializeField]
        private MeshSettings meshSettings;

        [SerializeField]
        private HeightMapSettings heightMapSettings;

        [SerializeField]
        private TextureData textureSettings;

        private Vector2 viewerPosition;
        private List<TerrainChunk> visibleTerrainChunks;

        [SerializeField]
        private int colliderLODIndex;

        [SerializeField]
        private LODInfo[] detailLevels;

        [SerializeField]
        private Material mapMaterial;

        private int chunksVisibleInViewDst;

        private Dictionary<Vector2, TerrainChunk> terrainChunkDictionary;
        private Vector2 viewerPositionOld;

        [SerializeField]
        private bool generateNavmesh = true;

        private NavmeshGenerator navmeshGenerator;

        private void Awake()
        {
            this.terrainChunkDictionary = new Dictionary<Vector2, TerrainChunk>();
            this.visibleTerrainChunks = new List<TerrainChunk>();
            this.navmeshGenerator = this.GetComponent<NavmeshGenerator>();

            float maxViewDst = this.detailLevels[this.detailLevels.Length - 1].visibleDstThreshold;
            this.chunksVisibleInViewDst = Mathf.RoundToInt(maxViewDst / this.meshSettings.MeshWorldSize);

            Transform navMeshModifierVolume = this.GetComponentInChildren<NavMeshModifierVolume>().transform;

            Vector3 navmeshVolumeScale = navMeshModifierVolume.localScale;

            navMeshModifierVolume.localScale = new Vector3(this.meshSettings.MeshWorldSize * (this.chunksVisibleInViewDst + 2), navmeshVolumeScale.y, this.meshSettings.MeshWorldSize * (this.chunksVisibleInViewDst + 2));
        }

        private void Start()
        {
            this.textureSettings.ApplyToMaterial(this.mapMaterial);
            this.textureSettings.UpdateMeshHeights(this.mapMaterial, this.heightMapSettings.MinHeight, this.heightMapSettings.MaxHeight);

            this.UpdateVisibleChunks();

            this.navmeshGenerator.worldRadius = this.meshSettings.MeshWorldSize * this.chunksVisibleInViewDst;
            if (this.generateNavmesh)
                this.navmeshGenerator.StartBuildingNavmesh(this.terrainChunkDictionary.Values.Select(chunk => chunk.NavmeshSurface));
        }

        private void Update()
        {
            Vector3 position = Vector3.zero;
            this.viewerPosition = new Vector2(position.x, position.z);

            if (this.viewerPosition != this.viewerPositionOld)
                foreach (TerrainChunk chunk in this.visibleTerrainChunks)
                    chunk.UpdateCollisionMesh();

            if ((this.viewerPositionOld - this.viewerPosition).sqrMagnitude > TerrainGenerator.sqrViewerMoveThresholdForChunkUpdate)
            {
                this.viewerPositionOld = this.viewerPosition;
                this.UpdateVisibleChunks();
            }
        }

        private void UpdateVisibleChunks()
        {
            HashSet<Vector2> alreadyUpdatedChunkCoords = new HashSet<Vector2>();
            for (int i = this.visibleTerrainChunks.Count - 1; i >= 0; i--)
            {
                alreadyUpdatedChunkCoords.Add(this.visibleTerrainChunks[i].Coord);
                this.visibleTerrainChunks[i].UpdateTerrainChunk();
            }

            int currentChunkCoordX = Mathf.RoundToInt(this.viewerPosition.x / this.meshSettings.MeshWorldSize);
            int currentChunkCoordY = Mathf.RoundToInt(this.viewerPosition.y / this.meshSettings.MeshWorldSize);

            for (int yOffset = -this.chunksVisibleInViewDst; yOffset <= this.chunksVisibleInViewDst; yOffset++)
            {
                for (int xOffset = -this.chunksVisibleInViewDst; xOffset <= this.chunksVisibleInViewDst; xOffset++)
                {
                    Vector2 viewedChunkCoord = new Vector2(currentChunkCoordX + xOffset, currentChunkCoordY + yOffset);

                    if (alreadyUpdatedChunkCoords.Contains(viewedChunkCoord))
                        continue;

                    if (this.terrainChunkDictionary.ContainsKey(viewedChunkCoord))
                    {
                        this.terrainChunkDictionary[viewedChunkCoord].UpdateTerrainChunk();
                    }
                    else
                    {
                        TerrainChunk newChunk = new TerrainChunk(
                            viewedChunkCoord,
                            this.heightMapSettings,
                            this.meshSettings,
                            this.detailLevels,
                            this.colliderLODIndex,
                            this.transform,
                            this.mapMaterial
                        );
                        this.terrainChunkDictionary.Add(viewedChunkCoord, newChunk);
                        newChunk.OnVisibilityChanged += this.OnTerrainChunkVisibilityChanged;
                        newChunk.Load();
                    }
                }
            }
        }

        private void OnTerrainChunkVisibilityChanged(TerrainChunk chunk, bool isVisible)
        {
            if (isVisible)
                this.visibleTerrainChunks.Add(chunk);
            else
                this.visibleTerrainChunks.Remove(chunk);
        }
    }

    [Serializable]
    public struct LODInfo
    {
        [Range(0, MeshSettings.NumSupportedLODs - 1)]
        public int lod;

        public float visibleDstThreshold;

        public float SqrVisibleDstThreshold { get => this.visibleDstThreshold * this.visibleDstThreshold; }
    }

}
