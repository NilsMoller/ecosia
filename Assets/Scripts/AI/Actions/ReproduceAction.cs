using System;
using System.Collections.Generic;

using AI.Agents;

using UnityEngine;

namespace AI.Actions
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Reproduction), typeof(Vision))]
    public class ReproduceAction : TimedActionBase
    {
        private Reproduction reproduction;
        private IGoapAgent agent;
        private Vision vision;
        private bool hasReproduced;
        private Reproduction nearestViableMate = null;

        public override bool IsDone { get => this.hasReproduced; }
        public override bool RequiresInRange { get => true; }

        public override float Cost { get => this.reproduction.CurrentDesire / this.reproduction.DesireThreshold * base.Cost; }

        protected override void Awake()
        {
            base.Awake();

            this.agent = this.GetComponent<IGoapAgent>();
            this.vision = this.GetComponent<Vision>();
            this.reproduction = this.GetComponent<Reproduction>();

            this.AddPrecondition(Condition.IsThirsty, false);
            this.AddPrecondition(Condition.IsHungry, false);

            this.AddPostcondition(Condition.WantsToReproduce, false);
        }

        protected override void Update()
        {
            base.Update();
            if (this.nearestViableMate != null)
                this.Target = this.nearestViableMate.transform.position;
        }

        protected override void OnReset()
        {
            base.OnReset();
            this.hasReproduced = false;
            this.reproduction.Partner = null;
            this.nearestViableMate = null;
        }

        public override bool CheckProceduralPreconditions()
        {
            List<Reproduction> mates = new List<Reproduction>();
            foreach (GameObject seenObject in this.vision.See())
            {
                if (seenObject.TryGetComponent(out Reproduction seenMate) && seenObject.GetComponent<IGoapAgent>().GetType() == this.agent.GetType())
                    mates.Add(seenMate);
            }

            float distanceToNearestMate = float.MaxValue;

            foreach (Reproduction mateCheck in mates)
            {
                if (!mateCheck.WantsToReproduce || !mateCheck.GetComponent<IGoapAgent>().CurrentGoal.ContainsKey(Condition.WantsToReproduce))
                    continue;
                if (mateCheck.Partner != null && mateCheck.Partner != this.reproduction)
                    continue;

                float distance = Vector3.Distance(mateCheck.transform.position, this.transform.position);

                if (distanceToNearestMate <= distance)
                    continue;

                distanceToNearestMate = distance;
                this.nearestViableMate = mateCheck;
            }

            if (this.nearestViableMate == null)
                return false;

            this.reproduction.Partner = this.nearestViableMate;

            if (!this.nearestViableMate.GetComponent<IGoapAgent>().CurrentGoal.ContainsKey(Condition.WantsToReproduce))
                this.nearestViableMate.GetComponent<Planner>().ForceAbortPlan(this);

            return true;
        }

        public override bool Perform()
        {
            if (!base.Perform()) // if the timer hasnt finished
                return true;

            try
            {
                this.reproduction.Reproduce(this.nearestViableMate);
                this.hasReproduced = true;
                return true;
            }
            catch (InvalidOperationException) // already mating/mated or no mate available
            {
                return false;
            }
        }
    }
}
