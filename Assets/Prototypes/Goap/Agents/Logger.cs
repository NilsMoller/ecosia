using System.Collections.Generic;

using AI;

using UnityEngine;

namespace Prototypes.Goap.Agents
{
    [DisallowMultipleComponent]
    public class Logger : WorkerBase
    {
        /// <summary>
        ///     Will chop trees.
        /// </summary>
        public override UniqueDictionary<Condition, bool> GetGoalState() => new UniqueDictionary<Condition, bool>
        {
            { Condition.CollectLogs, true },
        };
    }
}
