using System;
using System.Collections;

using UnityEngine;
using UnityEngine.AI;

using Logger = Utilities.Logger;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace Prototypes.WorldGeneration
{
    [RequireComponent(typeof(NavmeshGenerator))]
    public class Spawner : MonoBehaviour
    {
        private NavmeshGenerator navmeshGenerator;

        [SerializeField]
        private SpawnInfo[] spawns;

        private void Awake()
        {
            this.navmeshGenerator = this.GetComponent<NavmeshGenerator>();
            this.navmeshGenerator.OnNavmeshGenerated += this.OnNavmeshGenerated;
        }

        private void OnDisable()
        {
            this.navmeshGenerator.OnNavmeshGenerated -= this.OnNavmeshGenerated;
        }

        private void OnNavmeshGenerated()
        {
            this.StartCoroutine(this.SpawnCreatures());
        }

        private IEnumerator SpawnCreatures()
        {
            foreach (SpawnInfo spawnInfo in this.spawns)
            {
                for (int i = 0; i < spawnInfo.amountToSpawn; i++)
                {
                    bool foundPosition = false;
                    NavMeshHit navMeshHit;
                    do
                    {
                        foundPosition = NavMesh.SamplePosition(
                            Random.insideUnitSphere * this.navmeshGenerator.worldRadius,
                            out navMeshHit,
                            float.MaxValue,
                            NavMesh.AllAreas
                        );
                    } while (!foundPosition);

                    Vector3 spawnLocation = navMeshHit.position;
                    Object.Instantiate(
                        spawnInfo.objectToSpawn,
                        spawnLocation,
                        new Quaternion(
                            0,
                            Random.Range(-180f, 180f),
                            0,
                            0
                        )
                    );

                    yield return new WaitForEndOfFrame();
                }
            }

            Logger.Log("Finished spawning.", "green");
        }

        [Serializable]
        private struct SpawnInfo
        {
            public GameObject objectToSpawn;
            public int amountToSpawn;
        }
    }
}
