using UnityEngine;

namespace Prototypes.WorldGeneration.Data
{
    [CreateAssetMenu(menuName = "Terrain Generation/Mesh Settings")]
    public class MeshSettings : UpdatableData
    {
        public const int NumSupportedLODs = 5;
        private const int numSupportedChunkSizes = 9;
        private const int numSupportedFlatshadedChunkSizes = 3;

        private static readonly int[] supportedChunkSizes =
        {
            48,
            72,
            96,
            120,
            144,
            168,
            192,
            216,
            240,
        };

        public float meshScale = 2.5f;
        public bool useFlatShading;

        [Range(0, MeshSettings.numSupportedChunkSizes - 1)]
        public int chunkSizeIndex;

        [Range(0, MeshSettings.numSupportedFlatshadedChunkSizes - 1)]
        public int flatshadedChunkSizeIndex;

        /// <summary>
        /// Number of vertices per line of a mesh rendered at LOD = 0.
        /// </summary>
        /// <remarks>
        /// Includes the 2 extra vertices that are excluded from final mesh, but used for calculating normals.
        /// </remarks>
        public int NumVertsPerLine { get => MeshSettings.supportedChunkSizes[this.useFlatShading ? this.flatshadedChunkSizeIndex : this.chunkSizeIndex] + 5; } // + 1 for good normal calculation, + 5 for high res edges

        /// <summary>
        /// The size of one chunk.
        /// </summary>
        public float MeshWorldSize { get => (this.NumVertsPerLine - 3) * this.meshScale; }
    }
}
