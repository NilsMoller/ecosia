using AI;
using AI.Actions;

using Prototypes.Goap.Data;

using UnityEngine;

using Utilities.PrimitiveReferences;

namespace Prototypes.Goap.Actions.Wood
{
    [DisallowMultipleComponent]
    public class DropOffLogsAction : ActionBase
    {

        [SerializeField] private FloatReference workerInteractionRange = null;
        private bool droppedOffLogs;
        private SupplyPoint[] supplyPoints;
        private SupplyPoint targetSupplyPoint;

        public override bool IsDone { get => this.droppedOffLogs; }
        public override bool RequiresInRange { get => true; }

        public override bool IsInRange
        {
            get
            {
                if (!this.RequiresInRange)
                    return true;

                return Vector3.Distance(this.transform.position, this.targetSupplyPoint.transform.position) < this.workerInteractionRange;
            }
        }

        private new void Awake()
        {
            base.Awake();

            this.supplyPoints = Object.FindObjectsOfType<SupplyPoint>();

            this.AddPrecondition(Condition.HasLogs, true);
            this.AddPostcondition(Condition.HasLogs, false);
            this.AddPostcondition(Condition.CollectLogs, true); // this is basically what goal this fulfills
        }

        protected override void OnReset()
        {
            this.droppedOffLogs = false;
            this.targetSupplyPoint = null;
        }

        public override bool CheckProceduralPreconditions()
        {
            SupplyPoint nearestSupplyPoint = null;
            float distanceToNearestSupplyPoint = float.MaxValue;

            foreach (SupplyPoint supplyPointCheck in this.supplyPoints)
            {
                float distance = Vector3.Distance(supplyPointCheck.gameObject.transform.position, this.gameObject.transform.position);
                if (distanceToNearestSupplyPoint > distance)
                {
                    distanceToNearestSupplyPoint = distance;
                    nearestSupplyPoint = supplyPointCheck;
                }
            }

            if (nearestSupplyPoint == null)
                return false;

            this.targetSupplyPoint = nearestSupplyPoint;
            this.Target = nearestSupplyPoint.transform.position;

            return true;
        }

        public override bool Perform()
        {
            WorkerBackpack backpack = this.GetComponent<WorkerBackpack>();
            this.targetSupplyPoint.AmountOfLogs += backpack.AmountOfLogs;
            this.droppedOffLogs = true;
            backpack.AmountOfLogs -= backpack.AmountOfLogs;
            return true;
        }
    }
}
