using UnityEngine;

namespace Prototypes.WorldGeneration
{
    public class ConstantForwardMotion : MonoBehaviour
    {
        [SerializeField] private float speed = 5f;
    
        private Transform cachedTransform;

        private void Awake()
        {
            this.cachedTransform = this.transform;
        }

        private void Update()
        {
            this.cachedTransform.position += this.cachedTransform.forward * this.speed;
        }
    }
}
