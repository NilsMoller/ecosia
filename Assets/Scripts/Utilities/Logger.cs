using System;
using System.Collections.Generic;
using System.Linq;

using AI;
using AI.Actions;

using UnityEngine;

namespace Utilities
{
    public static class Logger
    {
        public static void Log(string message, string color = null, string extra = "")
        {
            if (message == null)
                throw new ArgumentException("Message cannot be null.");

            Debug.Log(color == null ? message : $"<color={color}>{message}</color>\n{extra ?? string.Empty}");
        }

        public static void Log(string message, UniqueDictionary<Condition, bool> dictionary, string color = null) => Logger.Log(message, color, Logger.Stringify(dictionary));

        public static void Log(UniqueDictionary<Condition, bool> dictionary, string color = null) => Logger.Log(Logger.Stringify(dictionary), color);

        public static void Log(IEnumerable<ActionBase> actions, string color = null) => Logger.Log(Logger.Stringify(actions), color);

        public static void Log(string message, ActionBase action, string color = null) => Logger.Log(message, color, action.ToString());

        public static void Log(ActionBase action, string color) => Logger.Log(action.ToString(), color);

        public static void Log(string message, IEnumerable<ActionBase> actions, string color = null) => Logger.Log(message, color, Logger.Stringify(actions));

        public static void Log(IEnumerable<object> array, string color = null) => Logger.Log(Logger.Stringify(array), color);

        public static void Log(string message, IEnumerable<object> array, string color = null) => Logger.Log(message, color, Logger.Stringify(array));

        public static string Stringify(UniqueDictionary<Condition, bool> dictionary)
        {
            return dictionary.Aggregate(string.Empty, (current, keyValuePair) => current + keyValuePair.Key + "\n");
        }

        public static string Stringify(IEnumerable<object> array)
        {
            return array.Aggregate(string.Empty, (current, item) => current + item + "\n");
        }
    }
}
