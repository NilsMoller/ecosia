using Prototypes.WorldGeneration.Data;

using Sirenix.OdinInspector;

using UnityEngine;

namespace Prototypes.WorldGeneration
{
    public class MapPreview : MonoBehaviour
    {
        public Renderer textureRenderer;
        public MeshFilter meshFilter;
        public MeshRenderer meshRenderer;

        public enum DrawMode { NoiseMap = 0, Mesh = 2, FalloffMap = 3 }

        public MeshSettings meshSettings;
        public HeightMapSettings heightMapSettings;
        public TextureData textureData;

        public Material terrainMaterial;

        public DrawMode drawMode;

        [Range(0, MeshSettings.NumSupportedLODs - 1)]
        public int editorPreviewLOD;
    
        private void OnValidate()
        {
            if (this.meshSettings != null)
            {
                this.meshSettings.OnValuesUpdated -= this.OnValuesUpdated;
                this.meshSettings.OnValuesUpdated += this.OnValuesUpdated;
            }

            if (this.heightMapSettings != null)
            {
                this.heightMapSettings.OnValuesUpdated -= this.OnValuesUpdated;
                this.heightMapSettings.OnValuesUpdated += this.OnValuesUpdated;
            }

            if (this.textureData != null)
            {
                this.textureData.OnValuesUpdated -= this.OnTextureValuesUpdated;
                this.textureData.OnValuesUpdated += this.OnTextureValuesUpdated;
            }
        }

        private void OnValuesUpdated()
        {
            if (!Application.isPlaying)
                this.DrawMapInEditor();
        }

        private void OnTextureValuesUpdated()
        {
            this.textureData.ApplyToMaterial(this.terrainMaterial);
        }

        [Button]
        public void DrawMapInEditor()
        {
            this.textureData.ApplyToMaterial(this.terrainMaterial);
            this.textureData.UpdateMeshHeights(this.terrainMaterial, this.heightMapSettings.MinHeight, this.heightMapSettings.MaxHeight);
            HeightMap heightMap = HeightMapGenerator.GenerateHeightMap(this.meshSettings.NumVertsPerLine, this.meshSettings.NumVertsPerLine, this.heightMapSettings, Vector2.zero);
            MapPreview preview = Object.FindObjectOfType<MapPreview>();
            switch (this.drawMode)
            {
                case DrawMode.NoiseMap:
                    preview.DrawTexture(TextureGenerator.TextureFromHeightMap(heightMap));
                    break;

                case DrawMode.Mesh:
                    preview.DrawMesh(
                        MeshGenerator.GenerateTerrainMesh(
                            heightMap.Values,
                            this.meshSettings,
                            this.editorPreviewLOD
                        )
                    );
                    break;

                case DrawMode.FalloffMap:
                    preview.DrawTexture(TextureGenerator.TextureFromHeightMap(new HeightMap(FalloffGenerator.GenerateFalloffMap(this.meshSettings.NumVertsPerLine), 0f, 1f)));
                    break;
            }
        }

        private void DrawTexture(Texture2D texture)
        {
            this.textureRenderer.sharedMaterial.mainTexture = texture;
            this.textureRenderer.transform.localScale = new Vector3(texture.width, 1, texture.height) / 10f;
        
            this.textureRenderer.gameObject.SetActive(true);
            this.meshFilter.gameObject.SetActive(false);
        }

        private void DrawMesh(MeshData meshData)
        {
            this.meshFilter.sharedMesh = meshData.CreateMesh();
        
            this.textureRenderer.gameObject.SetActive(false);
            this.meshFilter.gameObject.SetActive(true);
        }
    }
}
