using UnityEditor;

using UnityEngine;

using Logger = Utilities.Logger;

namespace AI
{
    public class Death : MonoBehaviour
    {
        public void Die(string reason = null)
        {
            if (Selection.Contains(this.gameObject))
                Logger.Log(this.gameObject.name + " died", "black", reason);

            Object.Destroy(this.gameObject);
        }
    }
}
