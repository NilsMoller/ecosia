using System.Collections.Generic;

using AI.Agents;

using Sirenix.OdinInspector;

using UnityEngine;

using Utilities.PrimitiveReferences;

namespace AI.Actions
{
    public abstract class ActionBase : MonoBehaviour
    {
        [SerializeField]
        private FloatReference actionCost = null;

        [SerializeField]
        [ShowIf(nameof(ActionBase.RequiresInRange))]
        protected FloatReference interactionRange = null;

        public abstract bool IsDone { get; }
        public abstract bool RequiresInRange { get; }

        public virtual float Cost { get => this.actionCost; }

        public UniqueDictionary<Condition, bool> Preconditions { get; private set; }
        public UniqueDictionary<Condition, bool> PostConditions { get; private set; }

        public Vector3 Target { get; protected set; }

        public virtual bool IsInRange
        {
            get
            {
                if (!this.RequiresInRange)
                    return true;

                // ReSharper disable once LocalVariableHidesMember
                Transform transform = this.transform;
                Vector3 localScale = transform.localScale;
                return Vector3.Distance(transform.position, this.Target) <= (localScale.x + localScale.z) / 2f + this.interactionRange;
            }
        }

        protected virtual void Awake()
        {
            this.Preconditions = new UniqueDictionary<Condition, bool>();
            this.PostConditions = new UniqueDictionary<Condition, bool>();
        }

        protected abstract void OnReset();

        public abstract bool CheckProceduralPreconditions();

        public abstract bool Perform();

        protected void AddPrecondition(Condition key, bool value)
        {
            this.Preconditions.Add(key, value);
        }

        protected void RemovePrecondition(Condition key)
        {
            if (this.Preconditions.ContainsKey(key))
                this.Preconditions.Remove(key);
        }

        protected void AddPostcondition(Condition key, bool value)
        {
            this.PostConditions.Add(key, value);
        }

        protected void RemovePostcondition(Condition key)
        {
            if (this.PostConditions.ContainsKey(key))
                this.PostConditions.Remove(key);
        }

        public void ResetAction()
        {
            this.Target = Vector3.zero;
            this.OnReset();
        }

        public override string ToString()
        {
            string[] names = this.GetType().ToString().Split('.');
            return names[names.Length - 1];
        }
    }
}
