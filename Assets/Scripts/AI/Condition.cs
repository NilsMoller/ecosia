namespace AI
{
    public enum Condition
    {
        HasOre = 0,
        HasLogs = 1,
        HasTool = 2,
        CollectOre = 3,
        CollectLogs = 4,
        CollectTools = 5,
        IsIdle = 11,
        WantsToReproduce = 12,
        IsThirsty = 13,
        IsHungry = 14,
        WantsToAvoidPredator = 15,
    }
}
