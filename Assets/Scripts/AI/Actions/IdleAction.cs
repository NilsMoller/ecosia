using UnityEngine;

using Utilities.PrimitiveReferences;

namespace AI.Actions
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(NavmeshMovement))]
    public class IdleAction : TimedActionBase
    {
        private NavmeshMovement movement;
        private bool finishedIdling;

        [SerializeField] private FloatReference maxWanderingRange;

        public override bool IsDone { get => this.finishedIdling; }
        public override bool RequiresInRange { get => true; }

        protected override void Awake()
        {
            base.Awake();

            this.movement = this.GetComponent<NavmeshMovement>();
            
            this.AddPostcondition(Condition.IsIdle, true);
        }

        protected override void OnReset()
        {
            base.OnReset();

            this.finishedIdling = false;
        }

        public override bool CheckProceduralPreconditions()
        {
            this.Target = this.movement.GetRandomDestination(this.maxWanderingRange);

            return true;
        }

        public override bool Perform()
        {
            if (!base.Perform())
                return true;

            this.finishedIdling = true;
            return true;
        }
    }
}
