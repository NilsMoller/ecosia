using System;
using System.Threading;

using Prototypes.WorldGeneration.Data;

using UnityEngine;
using UnityEngine.AI;

namespace Prototypes.WorldGeneration
{
    public class TerrainChunk
    {
        private readonly float colliderGenerationDistanceThreshold;
        public event Action<TerrainChunk, bool> OnVisibilityChanged;
        private readonly int colliderLODIndex;

        private readonly LODInfo[] detailLevels;
        private readonly LODMesh[] lodMeshes;
        private readonly MeshCollider meshCollider;
        private readonly MeshFilter meshFilter;
        private readonly GameObject meshObject;

        private readonly Vector2 sampleCenter;

        private Bounds bounds;
        public Vector2 Coord;
        private readonly HeightMapSettings heightMapSettings;
        private readonly MeshSettings meshSettings;
        private bool hasSetCollider;
        private readonly float maxViewDst;

        private HeightMap heightMap;
        private bool heightMapReceived;
        private int previousLODIndex;
        public readonly NavMeshSurface NavmeshSurface;

        public TerrainChunk(Vector2 coord,
                            HeightMapSettings heightMapSettings,
                            MeshSettings meshSettings,
                            LODInfo[] detailLevels,
                            int colliderLODIndex,
                            Transform parent,
                            Material material)
        {
            this.Coord = coord;
            this.heightMapSettings = heightMapSettings;
            this.meshSettings = meshSettings;
            this.previousLODIndex = -1;
            this.colliderLODIndex = colliderLODIndex;
            this.colliderGenerationDistanceThreshold = detailLevels[detailLevels.Length - 1].visibleDstThreshold;

            this.detailLevels = detailLevels;

            this.sampleCenter = coord * meshSettings.MeshWorldSize / meshSettings.meshScale;
            Vector2 position = coord * meshSettings.MeshWorldSize;
            this.bounds = new Bounds(position, Vector2.one * meshSettings.MeshWorldSize);

            this.meshObject = new GameObject("Terrain Chunk");
            this.meshFilter = this.meshObject.AddComponent<MeshFilter>();
            this.meshCollider = this.meshObject.AddComponent<MeshCollider>();
            this.meshObject.AddComponent<MeshRenderer>().material = material;
            this.NavmeshSurface = this.meshObject.AddComponent<NavMeshSurface>();
            this.NavmeshSurface.useGeometry = NavMeshCollectGeometry.PhysicsColliders;
            this.meshObject.transform.position = new Vector3(position.x, 0, position.y);
            this.meshObject.transform.parent = parent;
            this.meshObject.SetActive(false);

            this.lodMeshes = new LODMesh[detailLevels.Length];
            for (int i = 0; i < detailLevels.Length; i++)
            {
                this.lodMeshes[i] = new LODMesh(detailLevels[i].lod);
                this.lodMeshes[i].UpdateCallback += this.UpdateTerrainChunk;
                if (i == this.colliderLODIndex)
                    this.lodMeshes[i].UpdateCallback += this.UpdateCollisionMesh;
            }

            this.maxViewDst = detailLevels[detailLevels.Length - 1].visibleDstThreshold;
        }

        public void Load()
        {
            ThreadedDataRequester.RequestData(
                () => HeightMapGenerator.GenerateHeightMap(
                    this.meshSettings.NumVertsPerLine,
                    this.meshSettings.NumVertsPerLine,
                    this.heightMapSettings,
                    this.sampleCenter
                ),
                this.OnHeightMapReceived
            );
        }

        private void OnHeightMapReceived(object heightMapObject)
        {
            this.heightMap = (HeightMap)heightMapObject;
            this.heightMapReceived = true;

            this.UpdateTerrainChunk();
        }

        private Vector2 ViewerPosition { get => Vector3.zero; }

        public void UpdateTerrainChunk()
        {
            if (this.heightMapReceived)
            {
                float viewerDstFromNearestEdge = Mathf.Sqrt(this.bounds.SqrDistance(this.ViewerPosition));
                bool wasVisible = this.meshObject.activeSelf;
                bool visible = viewerDstFromNearestEdge <= this.maxViewDst;

                if (visible)
                {
                    int lodIndex = 0;

                    for (int i = 0; i < this.detailLevels.Length - 1; i++)
                    {
                        if (viewerDstFromNearestEdge > this.detailLevels[i].visibleDstThreshold)
                            lodIndex = i + 1;
                        else
                            break;
                    }

                    if (lodIndex != this.previousLODIndex)
                    {
                        LODMesh lodMesh = this.lodMeshes[lodIndex];
                        if (lodMesh.HasMesh)
                        {
                            this.previousLODIndex = lodIndex;
                            this.meshFilter.mesh = lodMesh.Mesh;
                        }
                        else if (!lodMesh.HasRequestedMesh)
                        {
                            lodMesh.RequestMesh(this.heightMap, this.meshSettings);
                        }
                    }
                }

                if (wasVisible != visible)
                {
                    this.meshObject.SetActive(visible);
                    this.OnVisibilityChanged?.Invoke(this, visible);
                }
            }
        }

        public void UpdateCollisionMesh()
        {
            if (this.hasSetCollider)
                return;

            float sqrDstFromViewerToEdge = this.bounds.SqrDistance(this.ViewerPosition);

            if (sqrDstFromViewerToEdge < this.detailLevels[this.colliderLODIndex].SqrVisibleDstThreshold)
                if (!this.lodMeshes[this.colliderLODIndex].HasRequestedMesh)
                    this.lodMeshes[this.colliderLODIndex].RequestMesh(this.heightMap, this.meshSettings);

            if (!this.lodMeshes[this.colliderLODIndex].HasMesh || !(sqrDstFromViewerToEdge < this.colliderGenerationDistanceThreshold * this.colliderGenerationDistanceThreshold))
                return;

            this.meshCollider.sharedMesh = this.lodMeshes[this.colliderLODIndex].Mesh;

            this.hasSetCollider = true;
        }

        private class LODMesh
        {
            private readonly int lod;
            public bool HasMesh;
            public bool HasRequestedMesh;
            public Mesh Mesh;

            public LODMesh(int lod)
            {
                this.lod = lod;
                this.HasRequestedMesh = false;
                this.HasMesh = false;
            }

            public event Action UpdateCallback;

            private void OnMeshDataReceived(object meshDataObject)
            {
                this.Mesh = ((MeshData)meshDataObject).CreateMesh();
                this.HasMesh = true;

                this.UpdateCallback?.Invoke();
            }

            public void RequestMesh(HeightMap heightMap, MeshSettings meshSettings)
            {
                this.HasRequestedMesh = true;
                ThreadedDataRequester.RequestData(() => MeshGenerator.GenerateTerrainMesh(heightMap.Values, meshSettings, this.lod), this.OnMeshDataReceived);
            }
        }
    }
}
